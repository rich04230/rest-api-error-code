# REST api spec

## Use Case: HIS project

### Return code
| Return Code | HTTP Status Code | Description |
| --- | --- | --- |
| 0000 | 200 | 成功 |
| 0001 | 200 | 請求欄位資料不完整(xxxx) |
| 0002 | 200 | 請求欄位資料格式不正確(xxxx) |
| 0003 | 200 | 會員資料不存在 |
| 0004 | 401 | 會員認證失敗 |
| 0005 | 403 | 會員沒有權限 |
| 0006 | 404 | 資料不存在 |
| 8999 | 500 | 發生資料庫錯誤 |
| 9999 | 500 | 發生未預期錯誤 |


### User login & Auth flow

```plantuml
@startuml
title 用戶 Token 流程
actor User
autonumber
participant User order 10
participant Browser order 20
participant BFF order 30
participant Member order 40
participant OAuth order 50

== Login API ==
Browser -> BFF: 發動 用戶帳號密碼登入
BFF -> BFF: 檢查 Access Token 合法性
BFF -> Member: 發動 用戶帳號密碼登入
Member -> OAuth: 發動 用戶帳號密碼登入
note left: 透過 Basic auth 驗證 Member, grant_type=password
OAuth -> OAuth: 檢查 Client 合法性
OAuth -> OAuth: 檢查 User 合法性
OAuth -> Member: 回覆登入結果
note left: 登入成功 核發 Access Token, Refresh Token
Member -> BFF: Token + 用戶資訊
BFF -> Browser: 回覆登入結果
Browser -> Browser: 處理登入結果
note left: 清除匿名 Token, 並儲存 Access Token, Refresh Token 到 localStorage
== Access Token expired ==
Browser -> BFF: 發動 API 請求 
note left: 將 Access Token 放在 Request Header Authorization Bearer 傳輸
BFF -> BFF: 檢查 Access Token 合法性
BFF -> Browser: 回覆 401 Token 已過期
Browser -> Browser: 檢查有無 Refresh Token
note left: 用戶 Token 有核發 Refresh Token
Browser -> BFF: 請求 Refresh Token
BFF -> Member: 使用 Refresh Token 申請新 Token
Member -> OAuth: 使用 Refresh Token 申請新 Token
note left: 透過 Basic auth 驗證 Member, grant_type=refresh_token
OAuth -> Member: 核發 Token
note left: 核發 Access Token, Refresh Token
Member -> BFF: 核發 Token
BFF -> Browser: 取得
Browser -> Browser: 儲存到 localStorage
Browser -> BFF: 發動 API 請求 
note left: 繼續原本的請求動作
== Refresh Token expired ==
Browser -> BFF: 發動 API 請求 
note left: 將 Access Token 放在 Request Header Authorization Bearer 傳輸
BFF -> BFF: 檢查 Access Token 合法性
BFF -> Browser: 回覆 401 Token 已過期
Browser -> Browser: 檢查有無 Refresh Token
note left: 用戶 Token 有核發 Refresh Token
Browser -> BFF: 請求 Refresh Token
BFF -> Member: 使用 Refresh Token 申請新 Token
Member -> OAuth: 使用 Refresh Token 申請新 Token
note left: 透過 Basic auth 驗證 Member, grant_type=refresh_token
OAuth -> OAuth: 檢查 Refresh Token 合法性
OAuth -> Member: 401 Refresh Token 無效
Member -> BFF: 401 Refresh Token 無效
BFF -> Browser: 401 Refresh Token 無效
Browser -> User: 請重新登入
@enduml
```

## Common error code & response
|  HTTP Status Code | Description |
| --- | --- |
| 200 | 成功 |
| 201 | 資料建立成功 |
| 401 | 認證失敗 |
| 403 | 沒有存取權限 |
| 404 | 資料不存在 |
| 500 | 發生非預期錯誤 |
| 503 | 服務無法存取 |